version: "3.5"

volumes:
  redis-data:
  solr-data:
  
services:
  solr:
    image: solr:8.7.0
    container_name: solr
    ports:
      - "${SOLR_PORT}:8983"
    networks:
      - dotstat_network
    entrypoint: 
      - docker-entrypoint.sh
      - solr
      - start
      - -c
      - -f
    volumes: 
      - solr-data:/var/solr

  redis:
    image: redis:5.0.3
    container_name: redis
    ports:
      - "${REDIS_PORT}:6379"
    volumes:
      - redis-data:/data
    networks:
      - dotstat_network

  config:
    image: siscc/dotstatsuite-config:master
    container_name: config
    volumes:
      - "./config/assets:/opt/assets"
      - "./config/configs:/opt/configs"
      - "./config/i18n:/opt/i18n"
    networks:
      - dotstat_network

  data-lifecycle-manager:
    image: siscc/dotstatsuite-data-lifecycle-manager:master
    container_name: data-lifecycle-manager
    ports:
      - "${DLM_PORT}:7002"
    environment:
      ROBOTS_POLICY: ${ROBOTS_POLICY_DLM}
      DEFAULT_TENANT: ${DEFAULT_TENANT_DLM}
      CONFIG_URL: http://config
      AUTH_SERVER_URL: "${PROTOCOL}://${KEYCLOAK_HOST}:${KEYCLOAK_PORT}"
      TRANSFER_SERVER_URL: "${PROTOCOL}://${HOST}:${TRANSFER_PORT}/${TRANSFER_API_VERSION}"
      AUTHZ_SERVER_URL: "${PROTOCOL}://${HOST}:${AUTH_PORT}/${AUTHZ_API_VERSION}"
    volumes:
      - "./config/assets:/opt/build/assets"
    networks:
      - dotstat_network

  data-explorer:
    image: siscc/dotstatsuite-data-explorer:master
    container_name: data-explorer
    ports:
      - "${DE_PORT}:80"
    environment:
      CONFIG_URL: http://config
      DEFAULT_TENANT: ${DEFAULT_TENANT_DE}
      AUTH_SERVER_URL: "${PROTOCOL}://${KEYCLOAK_HOST}:${KEYCLOAK_PORT}"
      AUTH_PROVIDER: ${AUTH_PROVIDER}
      GA_TOKEN: ${GA_TOKEN}
      GTM_TOKEN: ${GTM_TOKEN_DE}
      ROBOTS_POLICY: ${ROBOTS_POLICY_DE}
    volumes:
      - "./config/assets:/opt/build/assets"
    networks:
      - dotstat_network

  share:
    image: siscc/dotstatsuite-share:master
    container_name: share
    ports:
      - "${SHARE_PORT}:3007"
    environment:
      CONFIG_URL: http://config
      SITE_URL: "${PROTOCOL}://${HOST}:${SHARE_PORT}"
      REDIS_HOST: redis
      REDIS_PORT: 6379
      REDIS_DB: ${SHARE_DB_INDEX}
      SMTP_host: ${SMTP_HOST}
      SMTP_port: ${SMTP_PORT}
      SMTP_secure: ${SMTP_TLS}
      SMTP_auth_user: ${SMTP_USER}
      SMTP_auth_pass: ${SMTP_PASSWORD}
      MAIL_FROM: ${SHARE_MAIL_FROM}
      API_KEY: ${API_KEY_SHARE}
      SECRET_KEY: ${SECRET_KEY_SHARE}
    networks:
      - dotstat_network

  data-viewer:
    image: siscc/dotstatsuite-data-viewer:master
    container_name: data-viewer
    ports:
      - "${VIEWER_PORT}:80"
    environment:
      CONFIG_URL: http://config
      DEFAULT_TENANT: ${DEFAULT_TENANT}
      GA_TOKEN: ${GA_TOKEN}
      GTM_TOKEN: ${GTM_TOKEN_DV}
      ROBOTS_POLICY: ${ROBOTS_POLICY_DV}
    volumes:
      - "./config/assets:/opt/build/assets"
    networks:
      - dotstat_network

  sfs:
    image: siscc/dotstatsuite-sdmx-faceted-search:master
    container_name: sfs
    ports:
      - "${SFS_PORT}:80"
    restart: always
    environment:
      DEFAULT_TENANT: ${DEFAULT_TENANT}
      CONFIG_URL: http://config
      REDIS_HOST: redis
      REDIS_PORT: ${REDIS_PORT}
      SOLR_HOST: solr
      SOLR_PORT: ${SOLR_PORT}
      REDIS_DB: ${SFS_DB_INDEX}
      API_KEY: ${API_KEY_SFS}
    networks:
      - dotstat_network

networks:
  dotstat_network:
    name: dotstat_common_network