## SFS

- run `docker-compose -f sfs-test-compose.yml up`
- run `yarn test:full` or `yarn start:srv`

- sfs config: http://localhost:3007/admin/config?api-key=secret
- solr admin: http://localhost:8983/solr/#/

**warning: .env required to setup solr and redis host**
